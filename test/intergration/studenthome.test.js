const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../app");
const database = require("../../src/config/database");
const assert = require("assert");
const jwt = require("jsonwebtoken")
const pool = require('../../src/config/database')
const logger = require('../../src/config/config').logger

chai.should();
chai.use(chaiHttp);

//const CLEAR_DB = 'DELETE IGNORE FROM `user`'
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"
const CLEAR_USERS_TABLE = "DELETE FROM `user`;"




describe("StudentHome", function () {
  
	
  	describe("UC-201 Maak studentenhuis", function () {
		it("TC-201-1 Verplicht veld ontbreekt", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					// cityName is missing
					houseName: "Huis",
					postalCode: "4817",
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

    	it("TC-201-2 Invalide postcode", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "stadt",
					houseName: "Huis",
					postalCode: 4817,
					phoneNumber: "0681171454"
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
    	it("TC-201-3 Invalide telefoonnummer", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "stadt",
					houseName: "Huis",
					postalCode: "11111",
					phoneNumber: 0681171454
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

    	it("TC-201-4 Studentenhuis bestaat al op dit adres (bestaandpostcode/huisnummer)", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "stadt",
					houseName: "Huis",
					postalCode: "4706RX",
					phoneNumber: "23892",
          			houseNr: 11
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

    	it("TC-201-5Niet ingelogd)", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.send({
					cityName: "stadt",
					houseName: "Huis",
					postalCode: "4706AA",
					phoneNumber: "23892",
          houseNr: 19
				})
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
    	//TC-201-6Studentenhuis  succesvol toegevoegd
    	it("TC-201-6Studentenhuis  succesvol toegevoegd)", (done) => {
			chai.request(server)
				.post("/api/studenthome/")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName: "stadt",
					houseName: "Huis",
					postalCode: "4706RX",
					phoneNumber: "23892",
          houseNr: 11
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		}) 
	}),

  	describe("UC-202 Overzicht van studentenhuizen", function () {
    	it("TC-202-1 Toon nul studentenhuizen", (done) => {
				chai.request(server)
					.get("/api/studenthome/")
					.end((err, res) => {
						res.should.have.status(404)
						//ik heb hier een 404 en geen 200 code gedaan omdat een return met niks een 404 zou moeten geven
						done()
					})
			})
    	it("TC-202-2 show 2 different studenthomes", (done) => {
				chai.request(server)
					.get("/api/studenthome/")
					.query({ name: "Princenhage", city: "Breda" })
					.end((err, res) => {
						res.should.have.status(200)
						res.body.result.should.have.lengthOf(2)
						done()
					})
			})
  		}) 
		// //TC-202-3Toon studentenhuizen met zoekterm op niet-bestaande stad
		it("TC-202-3Toon studentenhuizen met zoekterm op niet-bestaande stad", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ city: "wrong" })
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})

		//TC-202-4Toon studentenhuizen met zoekterm op niet-bestaande naam
		it("TC-202-4Toon studentenhuizen met zoekterm op niet-bestaande naam", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "wrong" })
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})
		//TC-202-5Toon studentenhuizen met zoekterm op bestaande stad

		it("TC-202-5Toon studentenhuizen met zoekterm op bestaande stad", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ city: "Breda" })
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})

		it("TC-202-6Toon studentenhuizen met zoekterm op bestaande naam", (done) => {
			chai.request(server)
				.get("/api/studenthome/")
				.query({ name: "Princenhage" })
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})
  	}) 
	describe("UC-203 Details van studentenhuis", function () {
		it("TC-203-1 Studentenhuis-ID bestaat niet", (done) => {
			chai.request(server)
				.get("/api/studenthome/100")
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})
		it("TC-203-2Studentenhuis-ID bestaat", (done) => {
			chai.request(server)
				.get("/api/studenthome/2")
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})
	},

	describe("UC-204 Studentenhuis wijzigen", function () {
		it("TC-204-1 Verplicht veld ontbreekt", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					// cityName is missing,
					address: "Dubbeldam",
					houseName: "Huis",
					houseNr: 3,
					postalCode: "4817",
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
		it("TC-204-2 Invalide postcode", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName : "Breda",
					address: "Dubbeldam",
					houseNr: 3,
					houseName: "Huis",
					postalCode: 4817,
					phoneNumber: "0681171454",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
		it("TC-204-3 Invalide telefoonnummer", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName : "Breda",
					address: "Dubbeldam",
					houseNr: 3,
					houseName: "Huis",
					postalCode: "4817WH",
					phoneNumber: 0681171454
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
		it("TC-204-4 Studentenhuis bestaat niet", (done) => {
			chai.request(server)
				.put("/api/studenthome/1000")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName : "Breda",
					address: "Dubbeldam",
					houseNr: 3,
					houseName: "Huis",
					postalCode: "4817WH",
					phoneNumber: "0681171454"
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
		it("TC-204-5Niet ingelogd", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")
//				Login turned off				
//				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName : "Breda",
					address: "Dubbeldam",
					houseNr: 3,
					houseName: "Huis",
					postalCode: "4817WH",
					phoneNumber: "0681171454"
				})
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
		//TC-204-6Studentenhuis  succesvol gewijzigd

		it("TC-204-6Studentenhuis  succesvol gewijzigd", (done) => {
			chai.request(server)
				.put("/api/studenthome/1")			
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					cityName : "Breda",
					address: "Dubbeldam",
					houseNr: 3,
					houseName: "Huis",
					postalCode: "4817WH",
					phoneNumber: "0681171454"
				})
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})
	}),
	describe("UC-205 Studentenhuis verwijderen", function () {
		it("TC-205-1 studenthome doesnt exist", (done) => {
			chai.request(server)
				.delete("/api/studenthome/2000")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})
		it("TC-205-2Niet ingelogd", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1")
				//.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
		it("TC-205-3Actor is geen eigenaar", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})
		it("TC-205-4Studentenhuis  succesvol verwijderd", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})
	})
)
