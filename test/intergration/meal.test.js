const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../app");
const database = require("../../src/config/database");
const assert = require("assert");
const jwt = require("jsonwebtoken")
const pool = require('../../src/config/database')
const logger = require('../../src/config/config').logger


chai.should();
chai.use(chaiHttp);

//Adds the meals for the test
const INSERT_MEAL =
	"INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES" +
	"('1', 'Meal1', 'description1', 'ingriedients1', 'allergies1', '2021-05-16 11:30:19', '2021-05-18 11:22:19', '3', '1', '1', '6')," +
	"('2', 'Meal2', 'description2', 'ingriedients2', 'allergies2', '2021-05-15 11:23:33', '2021-05-18 11:22:19', '3', '2', '1', '6');"

//Adds the student homes for the tests (this has to be done here because it does the meal tests before the student home tests and they  )
const INSERT_STUDENTHOME =
    "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES" +
    '(1, "Princenhage", "Princenhage", 11, 1,"4706RX","061234567891","Breda"),' +
    '(2, "Princenhage", "Haagdijk", 4, 2, "4706RX","061234567891","Breda");'	

describe("Mealtime", function () {
    //Clears the database before and after every test
	before((done) => {
		pool.query(INSERT_STUDENTHOME, (err, rows, fields) => {
		  if (err) {
			logger.error(`before studenthome queries: ${err}`)
			done(err)
		  }
		  if (rows) {
			logger.debug(`before studenthome queries`)
			done()
		  }
		})
	  })

	  before((done) => {
		pool.query(INSERT_MEAL, (err, rows, fields) => {
			if (err) {
				logger.error(`before mealtime queries: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before mealtime queries`)
				done()
			}
		})
	})

    describe("UC-301 Maaltijd aanmaken", function () {
		it("TC-301-1 Verplicht veld ontbreekt", (done) => {
			chai.request(server)
				.post("/api/studenthome/2/meal")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    // name missing
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        //TC-301-2Niet ingelogd
        it("TC-301-1 Verplicht veld ontbreekt", (done) => {
			chai.request(server)
				.post("/api/studenthome/2/meal")
                //login turned off
				//.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
        //TC-301-3Maaltijdsuccesvol toegevoegd
        it("TC-301-3Maaltijdsuccesvol toegevoegd", (done) => {
			chai.request(server)
				.post("/api/studenthome/2/meal")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})
	})

    describe("UC-302 Maaltijd wijzigen", function () {
        it("TC-302-1 Mandatory field missing", (done) => {
			chai.request(server)
				.put("/api/studenthome/3/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    //name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        it("TC-302-2Niet ingelogd", (done) => {
			chai.request(server)
				.put("/api/studenthome/3/meal/1")
				//.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
        //TC-302-3Niet de eigenaar van de data
        it("TC-302-2Niet ingelogd", (done) => {
			chai.request(server)
				.put("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        it("TC-302-4Maaltijd bestaat niet", (done) => {
			chai.request(server)
                //maaltijd bestaat niet
				.put("/api/studenthome/3/meal/66")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        it("TC-302-5Maaltijdsuccesvol gewijzigd", (done) => {
			chai.request(server)
                //maaltijd bestaat niet
				.put("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    name: "test",
                    description : "test",
                    ingredients : "test",
                    allergies : "test",
                    price : 1,
                    maxParticipants : 10
				})
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})
    })
    describe("UC-303 Lijst van maaltijden opvragen", function () {
        it("TC-303-1Lijst van maaltijden geretourneerd", (done) => {
            chai.request(server)
				.get("/api/studenthome/1/meal")
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
        })
    })
    describe("UC-304 Details van een maaltijd opvragen", function () {
        it("TC-304-1Maaltijd bestaat niet", (done) => {
            chai.request(server)
				.get("/api/studenthome/1/meal/200")
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
        })

        it("TC-304-2Details van maaltijdgeretourneerd", (done) => {
            chai.request(server)
				.get("/api/studenthome/1/meal/1")
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
        })
    })
    describe("UC-305 Maaltijd verwijderen", function () {
        it("TC-305-2Niet ingelogd", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/1")
				//.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(401)
					done()
				})
		})
        it("TC-305-3Niet de eigenaar van de data", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        it("TC-305-4Maaltijd bestaat niet", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/500")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
        it("TC-305-5Maaltijdsuccesvol verwijderd", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})

    })
    
})
