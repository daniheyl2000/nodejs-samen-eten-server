process.env.DB_DATABASE = process.env.DB_DATABASE || 'studenthome'
process.env.NODE_ENV = 'testing'
process.env.LOGLEVEL = 'error'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../app')
const pool = require('../../src/config/database')

const jwt = require("jsonwebtoken")
const assert = require("assert")
const logger = require('../../src/config/config').logger
chai.should()
chai.use(chaiHttp)

chai.should()
chai.use(chaiHttp)

//const CLEAR_DB = 'DELETE IGNORE FROM `user`'
const CLEAR_DB = "DELETE IGNORE FROM `studenthome`;" + "DELETE IGNORE FROM `user`;" + "DELETE IGNORE FROM `meal`"
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"
const CLEAR_USERS_TABLE = "DELETE FROM `user`;"
//adds users
const INSERT_USER =
	"INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
	'(1, "first", "last", "name1@server.nl","1234567", "secret"),' +
	'(2, "second", "secondlast", "name2@server.nl","1234567", "secret");'



	//Clears the database before and after every test
	before((done) => {
		pool.query(CLEAR_DB, (err, rows, fields) => {
		  if (err) {
			logger.log("CLEARING")
			logger.error(`before CLEARING tables: ${err}`)
			done(err)
		  } else {
			logger.log("CLEARING")
			done()
		  }
		})
	  })
	  after((done) => {
		pool.query(CLEAR_DB, (err, rows, fields) => {
		  if (err) {
			logger.log("CLEARING 2")
			console.log(`after error: ${err}`)
			done(err)
		  } else {
			logger.log("CLEARING 2")
			logger.info('After FINISHED')
			done()
		  }
		})
	})

describe('Authentication', () => {
  before((done) => {
    // console.log('beforeEach')
    pool.query(INSERT_USER, (err, rows, fields) => {
      if (err) {
        console.log(`beforeEach CLEAR error: ${err}`)
        done(err)
      } else {
        done()
      }
    })
  })

  // After successful register we have a valid token. We export this token
  // for usage in other testcases that require login.
  // let validToken

  describe('UC101 Registation', () => {

    it('TC-101-1 Verplicht veld ontbreekt', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          lastname: ' LastName ',
          email: 'test@test.nl',
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          // res.body.should.be.a('object')
          done()
        })
    })

    it('TC-101-2 Invalide email adres', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 122212,
          studentnr: 1234567,
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
          done()
        })
    })


    it('TC-101-3 Invalide wachtwoord', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.com',
          studentnr: 1234567,
          password: 22
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
          done()
        })
    })

    it('TC-101-4 Gebruiker bestaat al', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'first',
          lastname: 'last',
          email: 'name1@server.nl',
          studentnr: 1234567,
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')

          const response = res.body
          done()
        })
    })

    it('TC-101-5 Gebruiker succesvol geregistreerd', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')

          const response = res.body
          response.should.have.property('token').which.is.a('string')
          // response.should.have.property('username').which.is.a('string')
          done()
        })
    })

    
  })

  describe('UC102 Login', () => {
    /**
     * This assumes that a user with given credentials exists. That is the case
     * when register has been done before login.
     */
     it("TC-102-1 Required field missing", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					// invalid
					email: 1,
					password: "TEST-PASS-1",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
    it("TC-102-2 Invalide email adres", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					email: 1,
					password: "TEST-PASS-1",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})
    it("TC-102-2 Invalide email adres", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					email: "TEST-LASTNAME-2-@MAIL.com",
					// invalid 
					password: 1,
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

    it("TC-102-4 Gebruiker bestaat niet", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
          //gebruiker die niet bestaad
					password: "TEST-PASS-1",
					email: "NON-EXISTANT-TEST-LASTNAME-2-@MAIL.com",
				})
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

    it("TC-102-5 Gebruiker succesvol ingelogd", (done) => {
			chai.request(server)
				.post("/api/login")
				.send({
					password: "secret",
					email: "name1@server.nl",
				})
				.end((err, res) => {
					res.should.have.status(200)
					done()
				})
		})

  })
})
