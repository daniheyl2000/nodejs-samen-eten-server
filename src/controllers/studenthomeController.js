const assert = require("assert");
var logger = require('tracer').console()
const studentHomeSql = require('../dao/sqlStudenthomeDao')
const errorStatus = require('./error_statuses')

module.exports = {
    validateStudenthome(req, res, next){
        console.log("validate studentome");
        console.log(req.body);
        try {
            const { houseName, address,postalCode, phoneNumber, cityName } = req.body;
            assert(typeof houseName === "string", "name is missing!");
            assert(typeof address === "string", "Address is missing!");
            assert(typeof postalCode === "string", "Postal_Code is missing!");
            assert(typeof phoneNumber === "string", "Telephone is missing!");
            assert(typeof cityName === "string", "City is missing!");
            console.log("Studenthome data is valid");
            next();
        } catch (error) {
            console.log("Studenthome data is Invalid: ", error.message);
            res.status(400);
            res.send(error.message);
        }
    },

    getAll: (req, res, next) => {
      logger.log("studenthomeController.findHomesByCity called!")

      const errorMessage = "Failed calling query!"

      logger.log(req.params)

      studentHomeSql.getAll(req,res,errorMessage,errorStatus.STATUS_400)

    },

    create: (req,res,next) => {
      logger.log("studenthomeController.Create called!")

      const errorMessage = "Failed calling query!"

      studentHomeSql.createHome(req,res,errorMessage,errorStatus.STATUS_400)
    },

    getById: (req,res,next) => {
      logger.log("studenthomeController.findHomesById called!")

      const homeId = req.params.homeId;
 
      const errorMessage = "Failed calling query!"

      studentHomeSql.getById(req,res,homeId,errorMessage,errorStatus.STATUS_400)
  },

  findHomesByCity: (req,res,next) => {
    logger.log("studenthomeController.findHomesByCity called!")
    
    const errorMessage = "Failed calling query!"

    logger.log(req.params)

    studentHomeSql.getByCity(req,res,errorMessage,errorStatus.STATUS_400)
  },
    
  changeById: (req,res,next) => {
    logger.log("studenthomeController.changeHomeById called!")

    const errorMessage = "Failed calling query!"

    logger.log(req.body)

    studentHomeSql.changeById(req,res,errorMessage,errorStatus.STATUS_400)

  },

  deleteById: (req,res,next) => {
    logger.log("studenthomeController.deleteHome called!")

    const homeId = req.params.homeId;

    const errorMessage = "Failed calling query!"

    studentHomeSql.deleteById(req,res,homeId,errorMessage,errorStatus.STATUS_400) 
  }

}
