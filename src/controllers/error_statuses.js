module.exports = { 
    STATUS_400 : 400,
    STATUS_404 : 404,
    STATUS_500 : 500
}