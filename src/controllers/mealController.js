const assert = require("assert")

const config = require('../config/config')
const loggerLog = config.logger

var logger = require('tracer').console()
const database = require('../config/database')
const pool = require('../config/database')
const sqlMealDao = require("../dao/sqlMealDao")
const errorStatus = require('./error_statuses');


module.exports = {

    findById: (req, res, next) => {
        logger.log("mealtimeController.findById called")

        const errorMessage = "Failed calling query!"

        sqlMealDao.getByMealId(
            req,
            res,
            errorMessage,
            errorStatus.STATUS_400
        )
    },

    findByHomeId: (req,res,next) => {
        logger.log("mealtimeController.findByHomeId called!")

        const errorMessage = "Failed calling query!"

        sqlMealDao.getByStudentHomeId(req,res,errorMessage,errorStatus.STATUS_400) 
    },

    delete: (req,res,next) => {
        logger.log("mealtimeController.deleteMeal called!")

        const errorMessage = "Failed calling query!"

        sqlMealDao.deleteById(req,res,errorMessage,errorStatus.STATUS_400) 
    },


    create: (req,res,next) => {
        logger.log("mealtimeController.createMeal called")

        const errorMessage = "Failed calling query!"

        sqlMealDao.createMeal(req,res,errorMessage,errorStatus.STATUS_400) 
    },

    change: (req,res,next) => {
        logger.log("mealtimeController.change called")

        const errorMessage = "Failed calling query!"

        sqlMealDao.changeMealById(req,res,errorMessage,errorStatus.STATUS_400) 
    }
}