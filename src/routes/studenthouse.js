const express = require("express");
const studenthomeController = require("../controllers/studenthomeController");
//const moviecontroller = require("../controllers/studenthomeController");
const router = express.Router();
const authController = require("../controllers/authentication.controller")

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

router.post("/studenthome",authController.validateToken, studenthomeController.validateStudenthome, studenthomeController.create);

router.get("/studenthome/:homeId", studenthomeController.getById);

router.get('/studenthome/', studenthomeController.findHomesByCity);

router.put('/studenthome/:homeId',authController.validateToken ,studenthomeController.validateStudenthome, studenthomeController.changeById)

router.delete('/studenthome/:homeId',authController.validateToken ,studenthomeController.deleteById)

module.exports = router;