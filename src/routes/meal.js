var logger = require('tracer').console()

const express = require("express")
const mealController = require('../controllers/mealController')
const authController = require('../controllers/authentication.controller')
const router = express.Router()

logger.log('Called class mealtime')

router.post("/:homeId/meal",authController.validateToken ,mealController.create) 

router.get('/:homeId/meal', mealController.findByHomeId)

router.get("/:homeId/meal/:mealId", mealController.findById)

router.put('/:homeId/meal/:mealId',authController.validateToken, mealController.change)

router.delete('/:homeId/meal/:mealId',authController.validateToken ,mealController.delete)

module.exports = router;