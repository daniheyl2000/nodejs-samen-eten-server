const assert = require("assert")

const config = require('../config/config')
const loggerLog = config.logger

var logger = require('tracer').console()
const database = require('../config/database')
const pool = require('../config/database')

module.exports = { 

    deleteExecute(err,connection,res,sqlQuery,errorMessage,errorStatus){
        if (err) {
            console.log("error")
            res.status(errorStatus).json({
                message: errorMessage, 
                error: err
            })
        }
        if (connection) {
            connection.query(sqlQuery, (error, results, fields) => {
                connection.release()
                if (error) {
                    res.status(errorStatus).json({
                        message: errorMessage,
                        error: error
                    })
                }
                if (results) {
                    logger.log('sqlQuery:' + sqlQuery)
                    
                    loggerLog.trace('results1: ', results[0])
                    loggerLog.trace('results2: ', results[1])
                    
                    
    
                    try{
                        // if getting   
                        const mappedResults = results.map((item) => {
                            return {
                                ...item,
                                user: {
                                    name: item.First_Name + ' ' + item.Last_Name
                                }
                            }
                        })

                        if(results[0].length > 0 && results[1].length > 0){
                            console.log("In try" + results) 
                            res.status(200).json({
                                result: mappedResults
                            })
                        }else{
                            res.status(404).json({
                                result:  'None found'
                            })
                        }

                        if (results.affectedRows <= 0) {
                            res.status(404).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }

                        
                        
    
                    } catch(err){
                        if (results.affectedRows <= 0) {
                            res.status(400).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }else{
                            console.log(results)
                            res.status(200).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }
                        // if returning after a delete or update
                    }
                }
            })
        }
    },

    basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus) {
        if (err) {
            res.status(errorStatus).json({
                message: errorMessage, 
                error: err
            })
        }
        if (connection) {
            connection.query(sqlQuery, (error, results, fields) => {
                connection.release()
                if (error) {
                    res.status(errorStatus).json({
                        message: errorMessage,
                        error: error
                    })
                }
                if (results) {
                    logger.log('sqlQuery:' + sqlQuery)
                    
                    loggerLog.trace('results: ', results)
                                        
    
                    try{
                        // if getting   
                        const mappedResults = results.map((item) => {
                            return {
                                ...item,
                                user: {
                                    name: item.First_Name + ' ' + item.Last_Name
                                }
                            }
                        })

                        if(results.length > 0){
                            console.log("In try" + results) 
                            res.status(200).json({
                                result: mappedResults
                            })
                        }else{
                            res.status(404).json({
                                result:  'None found'
                            })
                        }

                        if (results.affectedRows <= 0) {
                            res.status(404).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }

                        
                        
    
                    } catch(err){
                        if (results.affectedRows <= 0) {
                            res.status(400).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }else{
                            console.log(results)
                            res.status(200).json({
                                result:  results.affectedRows + ' affected'
                            })
                        }
                        // if returning after a delete or update
                    }
                }
            })
        }
    }
    
}

 