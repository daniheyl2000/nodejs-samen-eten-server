const assert = require("assert")

const config = require('../config/config')
const loggerLog = config.logger

var logger = require('tracer').console()
const database = require('../config/database')
const pool = require('../config/database')
const { create } = require("../controllers/studenthomeController")

const databaseExecutor = require('./sqlExecute')

module.exports = {

    getByMealId(req,res,errorMessage,errorStatus){
        const mealId = req.params.mealId
        const homeId = req.params.homeId

        const sqlQuery =
        'SELECT * ' +
        'FROM meal' +
        ' WHERE ID = ' + mealId +
        ' AND StudentHomeID = ' + homeId

        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    

    getByStudentHomeId(req,res,errorMessage,errorStatus){
        const homeId = req.params.homeId

        const sqlQuery =
        'SELECT * ' +
        'FROM meal ' +
        ' WHERE StudenthomeID = '  +  homeId

        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    changeMealById(req,res,errorMessage,errorStatus){

        const meal = req.body

        let { name, description, ingredients, allergies,price,maxParticipants} = meal

        const sqlQuery = 
        'UPDATE meal SET `Name` = ' + "'" + name             + "' " + 
        ', `Description` = '        + "'" +  description     + "' " + 
        ', `Ingredients` = '        + "'" + ingredients      + "' " +  
        ', `Allergies` = '          + "'" + allergies        + "' " + 
        ', `Price` = '              + "'" + price            + "' " + 
        ', `MaxParticipants` = '    + "'" + maxParticipants  + "' " +
        'WHERE ID = ' + req.params.mealId + ' AND StudenthomeID = ' + req.params.homeId +
        " AND UserID = " +
            req.userId 
        
        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    deleteById(req,res,errorMessage,errorStatus) {

        const mealId = req.params.mealId

        const sqlQuery =
        'DELETE ' +
        'FROM meal' +
        ' WHERE ID = ' + mealId +
        " AND UserID = " +
            req.userId
    
        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    createMeal(req,res,errorMessage,errorStatus){

        const meal = req.body
        let { name, description, ingredients, allergies,price,userId,maxParticipants} = meal
        let createdOn = new Date().getTime
        let offeredOn = new Date().getTime
        let studentHomeId = req.params.homeId
        const sqlQuery =
        'INSERT INTO `meal`(`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?,?,?,?,?,?,?,?,?,?)'
        
        logger.log(req.params)
        logger.log(req.body)
        logger.log(studentHomeId)
        logger.log(sqlQuery)

        pool.getConnection(function (err, connection) {
            if (err) {
              logger.error('createMeal', error)
              res.status(errorStatus).json({
                message: errorMessage,
                error: err
              })
            }
            if (connection) {
              // Use the connection
              connection.query(sqlQuery,
                [
                    name, description, ingredients, allergies, createdOn,offeredOn,price,req.userId,studentHomeId,maxParticipants
                ],
                (error, results, fields) => {
                  // When done with the connection, release it.
                  connection.release()
                  // Handle error after the release.
                  if (error) {
                    logger.error('mealtime', error.toString())
                    res.status(errorStatus).json({
                      message: errorMessage,
                      error: error.toString()
                    })
                  }
                  if (results) {
                    logger.trace('results: ', results)

                    res.status(200).json({
                      result: {
                        id: results.insertId,
                        ...meal
                      }
                    })
                  }
                }
              )
            }
          })
    }
}