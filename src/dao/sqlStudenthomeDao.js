const assert = require("assert")

const config = require('../config/config')
const loggerLog = config.logger

var logger = require('tracer').console()

const database = require('../config/database')
const pool = require('../config/database')

const databaseExecutor = require('./sqlExecute')

module.exports = {

    createHome(req,res,errorMessage,errorStatus){

        const studenthome = req.body
        let { houseName, address, houseNr, postalCode,phoneNumber,cityName} = studenthome


        const sqlQuery =
        'INSERT INTO `studenthome`(`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?,?,?,?,?,?,?)'
    
  
        pool.getConnection(function (err, connection) {
            if (err) {
              logger.error('createMovie', error)
              res.status(400).json({
                message: 'createMovie failed getting connection!',
                error: err
              })
            }
            if (connection) {
              // Use the connection
              connection.query(sqlQuery,
                [
                  houseName,
                  address,
                  houseNr,
                  req.userId,
                  postalCode,
                  phoneNumber,
                  cityName
                ],
                (error, results, fields) => {
                  // When done with the connection, release it.
                  connection.release()
                  // Handle error after the release.
                  if (error) {
                    logger.error('createStudenthome', error.toString())
                    res.status(errorStatus).json({
                      message: errorMessage,
                      error: error.toString()
                    })
                  }
                  //handels getting a valid result
                  if (results) {
                    logger.trace('results: ', results)

                    res.status(200).json({
                      result: {
                        id: results.insertId,
                        ...studenthome
                      }
                    })
                  }
                }
              )
            }
          })
    },
    //gets a home by ID
    getById(req,res,homeId,errorMessage,errorStatus){
        const sqlQuery =
        'SELECT * ' +
        'FROM studenthome ' +
        ' WHERE ID = '  +  homeId 

        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    getByCity(req,res,errorMessage,errorStatus){
        const cityName = req.query.city
        const houseName = req.query.name
        
        const sqlQuery =
        'SELECT * ' +
        'FROM studenthome ' +
        ' WHERE City = ' + '"' + cityName + '"' +
        ' OR Name = ' + '"' + houseName + '"'   

        pool.getConnection(function (err, connection) {
            databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
        })
    },

    deleteById(req,res,homeId,errorMessage,errorStatus) {
      const sqlQuery =
      'DELETE ' +
      'FROM meal' +
      ' WHERE StudentHomeID = '  +  homeId + ";"
      +'DELETE ' +
      'FROM studenthome' +
      ' WHERE ID = '  +  homeId +
      " AND UserID = " +
            req.userId

      pool.getConnection(function (err, connection) {
          databaseExecutor.deleteExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
      })
     
    },

    changeById(req,res,errorMessage,errorStatus){

      const homeId = req.params.homeId;

      const sqlQuery =
      'UPDATE studenthome ' +
      'SET ID = ' + homeId + 
      ', Name = ' +  '"' + req.body.houseName + '"' +
      ', Address = ' + '"' + req.body.address + '"' + 
      ', House_Nr = ' + req.body.houseNr  +
      ', UserId = ' + req.userId  +
      ', Postal_Code = ' + '"' + req.body.postalCode + '"' +
      ', Telephone = ' + '"' + req.body.phoneNumber + '"' +
      ', City = ' + '"' + req.body.cityName + '"' +
      ' WHERE ID = '  +  homeId +
      " AND UserID = " +
            req.userId

      pool.getConnection(function (err, connection) {
          databaseExecutor.basicExecute(err,connection,res,sqlQuery,errorMessage,errorStatus)
      })
  }


}