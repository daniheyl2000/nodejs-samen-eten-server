var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./src/routes/index');
var usersRouter = require('./src/routes/users');
const authroutes = require('./src/routes/authentication.routes')
var studenthomeRouter = require('./src/routes/studenthouse');
var mealRouter = require('./src/routes/meal');
//const mealController = require('./src/controllers/mealController');

var app = express();

// use case 103
app.get('/api/info', function (req, res) {
  let creator = {
    name: "Dani Heyl",
    studentNumber: '123456',
    description: 'My Nodejs server'
  }
  res.status(200).json(creator)
  //res.send('The creator of this app is Dani Hey!')
})



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Install the routes
app.use('/', indexRouter);
app.use('/api', authroutes)
app.use('/users', usersRouter);
app.use('/api', studenthomeRouter);
app.use("/api/studenthome", mealRouter)
// app.use('/api/studenthome/', mealRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
